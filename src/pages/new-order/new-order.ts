import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

@Component({
  selector: 'page-new-order',
  templateUrl: 'new-order.html'
})
export class NewOrderPage {

  constructor(public navCtrl: NavController) {
  }
  
}
