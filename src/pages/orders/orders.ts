import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { StorePage } from '../store/store';
import { OrderServiceProvider } from '../../providers/order-service/order-service';
import { OrderSinglePage } from '../order-single/order-single';

@Component({
  selector: 'page-orders',
  templateUrl: 'orders.html',
  providers: [OrderServiceProvider]
})
export class OrdersPage {
  public orders: any;

  constructor(
    public navCtrl: NavController,
    public orderService: OrderServiceProvider
  ) { }
  
  ionViewDidLoad() {
    this.loadOrders();
    console.log("View loaded for orders...");
   }

  loadOrders(){
    return new Promise(resolve => {
      this.orderService.getOrders()
      .then(data => {
        this.orders = data;
        resolve();
        //console.log('ITT A DATA===> ');
        //console.log(JSON.stringify(this.stores));
      })
      .catch(function(e){
        console.log(e);
      });
    })
  } 

  orderClick(order){
    this.navCtrl.push(OrderSinglePage, {
      order: order
    });
  }
}
