import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

@Component({
  selector: 'page-re-order',
  templateUrl: 're-order.html'
})
export class ReOrderPage {

  constructor(public navCtrl: NavController) {
  }
  
}
