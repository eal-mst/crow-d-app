import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { StoreServiceProvider } from '../../providers/store-service/store-service';

@Component({
  selector: 'page-store',
  templateUrl: 'store.html',
  providers: [StoreServiceProvider]
})
export class StorePage {

  public store: any;
  public products: any;
  constructor(public navCtrl: NavController, public navParams: NavParams, public storeService: StoreServiceProvider) {
    this.store = navParams.get('store');
    
  }

  ionViewDidLoad() {
    this.loadProducts(this.store.id);
    console.log("View loaded for store...");
   }

  loadProducts(id){
    return new Promise(resolve => {
      this.storeService.getProducts(id)
      .then(data => {
        this.products = data;
        resolve();
        console.log('ITT A PRODUCTS===> ');
        console.log(JSON.stringify(this.products));
      })
      .catch(function(e){
        console.log(e);
      });
    })
  } 
  
}
