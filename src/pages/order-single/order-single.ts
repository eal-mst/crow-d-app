import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { OrderServiceProvider } from '../../providers/order-service/order-service';
import { ProductPage } from '../product/product';

/**
 * Generated class for the OrderSinglePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-order-single',
  templateUrl: 'order-single.html',
  providers: [OrderServiceProvider]
})
export class OrderSinglePage {
  
  public order: any;
  public products: any
  constructor(public navCtrl: NavController, public navParams: NavParams, public orderService: OrderServiceProvider) {
    this.order = navParams.get('order');
  }

  ionViewDidLoad() {
    this.loadProducts(this.order.id);
    console.log('ionViewDidLoad OrderSinglePage');
  }

  loadProducts(id){
    return new Promise(resolve => {
      this.orderService.getProducts(id)
      .then(data => {
        this.products = data;
        resolve();
        console.log('ITT A PRODUCTS===> ');
        console.log(JSON.stringify(this.products));
      })
      .catch(function(e){
        console.log(e);
      });
    })
  } 

  productClick(prod){
    this.navCtrl.push(ProductPage, {
      product: prod
    });
  }

}
