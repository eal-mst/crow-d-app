import {
  GoogleMaps,
  GoogleMap,
  GoogleMapsEvent,
  GoogleMapOptions,
  CameraPosition,
  MarkerOptions,
  Marker
 } from '@ionic-native/google-maps';
import { Component } from "@angular/core/";
import { NavController, Platform } from 'ionic-angular';
import {StoreServiceProvider} from '../../providers/store-service/store-service';
import { StorePage } from '../store/store';
 
 @Component({
  selector: 'page-map',
  templateUrl: 'map.html',
  providers: [StoreServiceProvider]
})

 export class MapPage {
   map: GoogleMap;
   public stores: any;
   constructor(
     private googleMaps: GoogleMaps,
     public navCtrl: NavController,
     public plt: Platform,
     public storeService: StoreServiceProvider
    ) { }
 
   ionViewDidLoad() {
    this.loadMap();
   }
   
  loadStores(){
    return new Promise(resolve => {
      this.storeService.getStores()
      .then(data => {
        this.stores = data;
        resolve();
        //console.log('ITT A DATA===> ');
        //console.log(JSON.stringify(this.stores));
      })
      .catch(function(e){
        console.log(e);
      });
    })
  } 
 
  loadMap() {
 
     let mapOptions: GoogleMapOptions = {
       camera: {
         target: {
           lat: 55.4003461,
           lng: 10.3835165
         },
         zoom: 15,
         tilt: 0
       }
     };
 
     this.map = GoogleMaps.create('map_canvas', mapOptions);
 
     // Wait the MAP_READY before using any methods.
     this.map.one(GoogleMapsEvent.MAP_READY)
       .then(() => {
         console.log('Map is ready!');
         this.loadStores().then(() => {
          // Now you can use all methods safely.
         Object.keys(this.stores).forEach(key=> {
          let image = {
            url: this.stores[key].img_url,
            size: {
              width: 32,
              height: 32
            }
          };
          this.map.addMarker({
            title: this.stores[key].name,
            icon: image,
            animation: 'DROP',
            position: {
              lat: this.stores[key].latitude,
              lng: this.stores[key].longitude
            }
          })
          .then(marker => {
            marker.on(GoogleMapsEvent.MARKER_CLICK)
              .subscribe(() => {
                this.navCtrl.push(StorePage, {
                  store: this.stores[key]
                });
              });
          });
         })
        }); 
       });
   }
 }