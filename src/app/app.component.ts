import { Component, ViewChild } from '@angular/core';
import { Platform, Nav } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { Storage } from '@ionic/storage';

import { MapPage } from '../pages/map/map';
import { OrdersPage } from '../pages/orders/orders';
import { StorePage } from '../pages/store/store';


import { TabsControllerPage } from '../pages/tabs-controller/tabs-controller';



@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) navCtrl: Nav;
    rootPage:any = TabsControllerPage;

  constructor(platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen, private storage: Storage) {
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();

      //Set API URL in SQLite
      storage.get('API_URL').then((val) => {
        if(val == null){
          storage.set('API_URL', 'https://crowd.radioland.hu/api/');
          console.log('API URL freshly set... ', val);
        }else{
          console.log('API URL: ', val);
        }
      });
    });
  }
  /*goToMap(params){
    if (!params) params = {};
    this.navCtrl.setRoot(MapPage);
  }goToOrders(params){
    if (!params) params = {};
    this.navCtrl.setRoot(OrdersPage);
  }goToStore(params){
    if (!params) params = {};
    this.navCtrl.setRoot(StorePage);
  }*/
}
