import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';
import { MapPage } from '../pages/map/map';
import { OrdersPage } from '../pages/orders/orders';
import { OrderSinglePage } from '../pages/order-single/order-single';
import { NewOrderPage } from '../pages/new-order/new-order';
import { StorePage } from '../pages/store/store';
import { ProductPage } from '../pages/product/product';
import { ReOrderPage } from '../pages/re-order/re-order';
import { ProfilPage } from '../pages/profil/profil';
import { TabsControllerPage } from '../pages/tabs-controller/tabs-controller';
import { HttpClientModule } from '@angular/common/http';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { GoogleMaps } from '@ionic-native/google-maps';
import { StoreServiceProvider } from '../providers/store-service/store-service';
import { IonicStorageModule } from '@ionic/storage';
import { OrderServiceProvider } from '../providers/order-service/order-service';

@NgModule({
  declarations: [
    MyApp,
    MapPage,
    OrdersPage,
    OrderSinglePage,
    NewOrderPage,
    StorePage,
    ProductPage,
    ReOrderPage,
    ProfilPage,
    TabsControllerPage
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    IonicModule.forRoot(MyApp, {
      tabsHideOnSubPages:false
    }),
    IonicStorageModule.forRoot({
      name: '__mydb',
         driverOrder: ['sqlite', 'indexeddb', 'websql']
    })
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    MapPage,
    OrdersPage,
    OrderSinglePage,
    NewOrderPage,
    StorePage,
    ProductPage,
    ReOrderPage,
    ProfilPage,
    TabsControllerPage
  ],
  providers: [
    GoogleMaps,
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    StoreServiceProvider,
    OrderServiceProvider
  ]
})
export class AppModule {}