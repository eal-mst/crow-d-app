import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Storage } from '@ionic/storage';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

/*
  Generated class for the StoreServiceProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class StoreServiceProvider {
  public data: any;
  public url: string;

  constructor(public http: HttpClient, private storage: Storage) {
    console.log('Hello StoreServiceProvider Provider');
  }

  getStores() {
    return new Promise(resolve => {
      // don't have the data yet
      this.storage.get('API_URL').then((val) => {
        this.http.get(val+'store/').subscribe(data => {
          //console.log(JSON.stringify(data));
          resolve(data);
        }, err => {
          console.log(err);
        });
      });
    });
  }

  getProducts(id:any) {
    return new Promise(resolve => {
      // don't have the data yet
      this.storage.get('API_URL').then((val) => {
        this.http.get(val+'store/read/'+id+"/products").subscribe(data => {
          //console.log(JSON.stringify(data));
          resolve(data);
        }, err => {
          console.log(err);
        });
      });
    });
  }
}
